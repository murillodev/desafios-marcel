Test Driven Development:
O TDD transforma o desenvolvimento, pois deve-se primeiro escrever os testes, antes de implementar o sistema.
Os testes são utilizados para facilitar no entendimento do projeto, os testes são usados para clarear a ideia em relação ao que se deseja em relação ao código.
A criação de teste unitários ou de componentes é parte crucial para o TDD.
Os componentes individuais são testados para garantir que operem corretamente.
Cada componente é testado independentemente, sem os outros componentes de sistema.
Os componentes podem ser entidades simples, como funções ou classes de objetos, ou podem ser grupos coerentes dessas entidades”.
O principal benefício de usar o TDD é que torna o processo mais confiável, reduz custos de desenvolvimento devido ao
numero pequeno de bugs.